---
title: Freedom-Happiness
date: 2016-12-12
summary: Freedom leads to happiness
image_preview: edward-snowden.jpg
tags: freedom, snowden
---

> I'm incredibly fortunate and I think the greatest freedom that I've gained is the fact that I no longer have to worry about what happens tomorrow because I'm happy with what I've done today. - **Edward Snowden**
