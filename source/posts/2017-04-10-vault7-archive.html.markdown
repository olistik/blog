---
title: Vault7 Archive
date: 2017-04-10
summary: How to download all Vault7 documents published so far.
image_preview: vault7.jpg
---

<span class="image center">
  <img src="/images/vault7.jpg" alt="Vault7 Year Zero" />
</span>

How to download all Vault7 documents published so far.

On March 7, 2017, Wikileaks tweeted:

> ENCRYPTED RELEASE
> Use a 'torrent' downloader on:
> https://file.wikileaks.org/torrent/WikiLeaks-Year-Zero-2017-v1.7z.torrent
> And '7z' to decrypt.
> Passphrase will be made public at Tue 9am ET.

[tweet](https://twitter.com/wikileaks/status/839100031256920064)

Shortly followed by the revealing tweet:

> RELEASE: CIA Vault 7 Year Zero decryption passphrase:

`SplinterItIntoAThousandPiecesAndScatterItIntoTheWinds`

[tweet](https://twitter.com/wikileaks/status/839100031256920064)

It's roughly a 513MB 7zip archive.

You can read the whole content online as well, on the Wikileaks web site: [link](https://wikileaks.org/ciav7p1/).
