---
title: Carl Malamund on Aaron Swartz
date: 2017-07-16
summary: Some wise words that should put the law and the path to real freedom in the right perspective.
image_preview: malamund-swartz.jpg
---

<span class="image center">
  <img src="/images/malamund-swartz.jpg" alt="Carl Malamund on Aaron Swartz" />
</span>

[Carl Malamund](https://twitter.com/carlmalamud) remembers [Aaron Swartz](https://en.wikipedia.org/wiki/Aaron_Swartz):

> When we turn armed agents of the law on citizens trying to increase access to knowledge, we've broken the rule of law, we've desacrated the temple of justice.

> Change does not roll in on the wheels of inevitability, it comes through continuous struggle.
