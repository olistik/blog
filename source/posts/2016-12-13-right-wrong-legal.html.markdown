---
title: Right and Wrong, Legal and Illegal
date: 2016-12-13
summary: "\"we should always make a distinction that right and wrong is a very different standard than legal and illegal\""
image_preview: snowden-law-is-no-substitute-for-morality.jpg
---

**THIS** is possibly one of the most important contribute of the modern era. Can't stress enough how important this message is.

> we should always make a distinction that right and wrong is a very different standard than legal and illegal.

<span class="image main">
  <img src="/images/snowden-law-is-no-substitute-for-morality.jpg" alt="The law is no substitute for morality" />
</span>

[Tweet](https://twitter.com/Snowden/status/808370895030865920)

[Face-to-face with Edward Snowden in Moscow on Trump, Putin and dwindling hopes of a presidential pardon](https://www.yahoo.com/katiecouric/exclusive-face-to-face-with-edward-snowden-in-moscow-on-trump-putin-and-dwindling-hopes-of-a-presidential-pardon-100117331.html)
