---
title: Black Mirror is now
date: 2017-03-14
summary: Or "Another viable and likely, yet depicable, way for Facebook to generate revenues on the back of its users."
image_preview: black-mirror.jpg
---

<span class="image center">
  <img src="/images/black-mirror.jpg" alt="Shattered words" />
</span>

_Or "Another viable and likely, yet depicable, way for Facebook to generate revenues on the back of its users."_

A potential customer of the "hypothetical" _Facebook Premium Advertising Platform_ could at first create a set of psycogram filters, therefore targeting a specific userbase, then it could not only inject sponsored content (boring!) but it could apply more sensible modifications to the timelines of the target userbase, such as:

- suppressing notifications matching specific psycograms
- automatic promotion or demotion of posts matching specific psycograms or belonging to authors matching specific psycograms

Enjoy.

## References

- [Facebook performing psychological experiments on ~700k users, suppressing notifications](http://m.pnas.org/content/111/24/8788.full) (Paper)
- Psycograms: ["We have psycograms of all adult US citizens - 220 million people"](https://www.dailykos.com/story/2016/12/4/1607353/--We-have-psycograms-of-all-adult-US-citizens-220-million-people-from-their-facebook-likes), [German article](https://www.dasmagazin.ch/2016/12/03/ich-habe-nur-gezeigt-dass-es-die-bombe-gibt/)

