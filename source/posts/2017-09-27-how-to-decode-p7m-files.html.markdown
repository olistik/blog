---
title: How to decode p7m files
date: 2017-09-27
summary: Decoding .p7m files via the command line, using openssl.
---

Let's suppose that you have received a `document.pdf.p7m` via e-mail and you want to read it.

One way to do so, via the command line, is by using **openssl** (courtesy of `man 1 smime`):

> The smime command handles S/MIME mail. It can encrypt, decrypt, sign
and verify S/MIME messages.
>
> `-verify`
    verify signed mail. Expects a signed mail message on input and
    outputs the signed data. Both clear text and opaque signing is
    supported.
>
> `-in filename`
    the input message to be encrypted or signed or the MIME message to
    be decrypted or verified.
>
> `-inform SMIME|PEM|DER`
    this specifies the input format for the PKCS#7 structure. The
    default is SMIME which reads an S/MIME format message. PEM and DER
    format change this to expect PEM and DER format PKCS#7 structures
    instead. This currently only affects the input format of the
    PKCS#7 structure, if no PKCS#7 structure is being input (for
    example with -encrypt or -sign) this option has no effect.
>
> `-noverify`
    do not verify the signers certificate of a signed message.
>
> `-out filename`
    the message text that has been decrypted or verified or the output
    MIME format message that has been signed or verified.

Chaining everything together we end up with:

```
openssl smime -verify -in document.pdf.p7m -inform der -noverify -out document.pdf
```

That will produce `document.pdf`.

[Source](http://enricorossi.org/blog/2016/view_p7m_file_with_openssl/)
