---
title: La Privacy nei Social Network
date: 2017-01-22
summary: Targeted Advertising e Filter Bubble violano indirettamente la privacy degli utenti di un Social Network
image_preview: shattered-words.jpg
---

<span class="image center">
  <img src="/images/shattered-words.jpg" alt="Shattered words" />
</span>

La privacy di una comunicazione si basa sull'assunto che gli interlocutori non condividano al di fuori del contesto di partenza le informazioni scambiate.

Mano a mano che aumenta il numero di partecipanti diventa meno realistico assicurarsi che un'informazione non fuoriesca dal contesto di condivisione.

In base alla sensibilità di un informazione, decido se e con chi condividerla e accetto le regole del gioco, ovvero che la segretezza dell'informazione condivisa dipende *solo* dalla volontà delle persone con le quali ho condiviso tale informazione.

Ma lo strumento comunicativo che stiamo usando ora, Facebook, introduce altri meccanismi che non rispettano la nostra privacy in maniera *indiretta*. La violazione della nostra privacy è relativamente meno evidente.

Facebook inserisce pesantemente nella nostra esperienza sociale:

1. la pubblicità mirata ([Targeted Advertising](https://en.wikipedia.org/wiki/Targeted_advertising))
2. la manipolazione del flusso informativo che leggiamo ([Filter Bubble](https://en.wikipedia.org/wiki/Filter_bubble))

Cosa c'entra questo con la privacy? Il risultato delle nostre interazioni sociali, pubbliche o private viene usato per creare modelli psicologici personalizzati e tali modelli vengono dati in pasto alle due strategie menzionate (Targeted Advertising e Filter Bubble). Avviene quindi una violazione della nostra privacy in maniera *indiretta*, perché ciò che facciamo (chat, "Mi Piace", condivisioni e ogni altro tipo di meta dato) viene trasformato in un modello di ciò che siamo, che a sua volta viene usato per influenzare le nostre opinioni e conseguentemente anche le nostre decisioni, attraverso la taratura puntuale del contenuto che visualizziamo.

Non dobbiamo sopravvalutare le nostre capacità, ognuno può subire, anche senza rendersene conto, questo plagio. Le tecniche di analisi e di marketing comportamentale ([Behavioral Targeting](https://en.wikipedia.org/wiki/Behavioral_targeting)) sono molto, molto avanzate ed efficaci.

*Se* la piattaforma sociale non implementasse la Filter Bubble e *se* non venisse iniettato il Targeted Advertising, allora gli utenti potrebbero preoccuparsi solo di valutare la fiducia dell'insieme degli interlocutori del contesto di condivisione scelto (chat diretta, chat di gruppo o gruppo Facebook) in relazione alla sensibilità dell'informazione che si vuole condividere.

È per questi motivi che è importante valutare costantemente le alternative a Facebook più rispettose della nostra privacy.

Alcuni esempi di tali piattaforme virtuose, anche se ovviamente meno diffuse:

1. [Mastodon](https://mastodon.social)
2. [GNU Social](https://www.gnu.org/software/social/), un network decentralizzato con cui Mastodon è compatibile
3. [Ello](https://ello.co)

In soldoni: le condizioni necessarie, anche se non sufficienti, sono che una piattaforma non si basi sulla pubblicità mirata e non usi le informazioni non pubbliche per alimentare i modelli psicologici personalizzati.
