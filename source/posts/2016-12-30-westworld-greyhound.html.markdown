---
title: Westworld, greyhound story
date: 2016-12-30
summary: That dog had spent its whole life trying to catch that thing. Now it had no idea what to do.
image_preview: westworld-greyhound-quote.png
---

<span class="image center">
  <img src="/images/westworld-greyhound-quote.png" alt="A scene from Westworld S01E05" />
</span>

**Old Bill**: You got any stories, friend?

**Robert Ford**: Yeah, I suppose I do. You want to know the saddest thing I ever saw? When I was a boy, my brother and I wanted a dog, so our father took in an old greyhound. You've never seen a greyhound, have you, Bill?

**Old Bill**: Seen a few showdowns in my day.

**Robert Ford**: A greyhound is a racing dog. Spends its life running in circles, chasing a bit of felt made up like a rabbit. One day, we took it to the park. Our dad had warned us how fast that dog was, but we couldn't resist. So, my brother took off the leash, and in that instant, the dog spotted a cat. I imagine it must have looked just like that piece of felt. He ran. Never saw a thing as beautiful as that old dog running. Until, at last, he finally caught it. And to the horror of everyone, he killed that little cat. Tore it to pieces. Then he just sat there, confused. **That dog had spent its whole life trying to catch that thing. Now it had no idea what to do.**
