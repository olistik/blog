---
title: Knowledge
date: 2016-12-14
summary: Knowledge
image_preview: knowledge.png
---

[Hugh MacLeod](https://twitter.com/hughcards) described the concepts of _information_ and _knowledge_ with this image:

<span class="image center">
  <a href="https://twitter.com/hughcards/status/423952995240648704" target="_BLANK">
    <img src="/images/hugh-macleod-information-knowledge.jpg" alt="Information and Knowledge by Hugh MacLeod" />
  </a>
</span>

[Source tweet](https://twitter.com/hughcards/status/423952995240648704)

This image has been wrongly interpreted in several contexts, for example by translating _information_ with _knowledge_ and _knowledge_ itself with _experience_.
I find this interpretation misleading because _knowledge_ is a set of *patterns*, that is dots *and* lines, and _experience_ is an incremental process of enriching _knowledge_.

Based on Hugh MacLeod's concept of _information_ we should treat dots more like a set of raw values, like this one:

<div class="image center">
  <img src="/images/raw-values.png" alt="Information is a set of raw values" />
</div>

Then if we consider _knowledge_ instead of _information_ I see the following image as a better representation:

<div class="image center">
  <img src="/images/knowledge.png" alt="Knowledge and Experience by @olistik" class="background--white" />
</div>

with _experience_ playing the role of the differences between two snapshots of oneself's _knowledge_.
