---
title: Lifespan of web links matters
date: 2016-12-15
summary: Lifespan of web links matters, especially when you rely on or reference external resources.
image_preview: xanadu-mockup.jpg
---

<span class="image center">
  <img src="/images/xanadu-mockup.jpg" alt="Xanadu mockup" />
  <em>
    Image taken from the [Xanadu project](http://www.xanadu.com.au/ted/XUsurvey/xuDation.html) by [Ted Nelson](https://twitter.com/TheTedNelson).
  </em>
</span>


Lifespan of web links (URLs) matters, especially when they reference *external* resources.

We should care about URLs of resorces we publish on our own as well.

It would be ideal to let referencers subscribe to updates made to the resources they're referencing.

Each time a page gets rendered, an agent could then scan for external URLs and check whether or not the remote content is still available and even when it was last updated.

The results of this scans could then trigger some notifications:

- the reader of the page can identify broken links, for example by appropriately styling them
- the owner of the page is aware of both the presence and location of broken links
- the owners of the referenced resources can be notified as well so that they can try to fix their URLs, for example by restoring the resource or issuing a redirect
